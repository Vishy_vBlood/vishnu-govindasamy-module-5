import 'package:flutter/material.dart';
import 'package:myhash5app/main.dart';
import 'package:myhash5app/profileedit.dart';

class HomeScreem extends StatelessWidget {
  const HomeScreem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Dashboard")),
      body: Column(
        children: [
          FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return const profileed();
                  },
                ),
              );
            },
            child: const Text("Profile"),
          ),
          const SizedBox(
            height: 5,
          ),
          FloatingActionButton(
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return const LoginScreen();
                  },
                ),
                (route) => false,
              );
            },
            child: const Text("Sign\nOut"),
          ),
        ],
      ),
    );
  }
}
